import  OPi.GPIO as GPIO
from socket import *
import time
import sys


def main():
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(12,GPIO.OUT)
    GPIO.output(12,True)
    # Create a TCP/IP socket
    sock = socket(AF_INET, SOCK_DGRAM)

    # Bind the socket to the port
    server_address = ('192.168.0.141', 8888)
    print(sys.stderr, 'starting up on %s port %s' % server_address)
    sock.bind(server_address)

    while True:
        # print >> sys.stderr, '\nwaiting to receive message'
        data, address = sock.recvfrom(4096)

        print('received %s bytes from %s' % (len(data), address))
        # print >> sys.stderr, data
        print('data: %s' % data.decode("utf-8"))
        if data:
            sent = sock.sendto(data, address)
        # print >> sys.stderr, 'sent %s bytes back to %s' % (sent, address)


if __name__ == "__main__":
    main()
