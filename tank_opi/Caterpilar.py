from Useful import *

class Caterpilar:
    speed=0
    direction=LEFT
    out_speed=None
    out_dir=None
    def __init__(self,out_speed,out_dir):
        out_speed=out_speed
        out_dir=out_dir
        set_output(out_dir)
        set_output(out_speed)
